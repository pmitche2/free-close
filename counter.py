class Counter:
  """Counts Proper Nouns"""

  name_count = 0
  pronouns_count = 0
  pronouns_dobj_count = 0

  def document(self, doc):
    self.doc = doc
    for token in self.doc:
      self.process_token(token)

    for ent in self.doc.ents:
      if ent.label_ == "PERSON":
        self.name_count += 1

  def process_token(self, token):
    method = self.lookup_method(token.pos_)
    if method:
      return method(token)

  def lookup_method(self, pos):
    return getattr(self, 'do_' + pos, None)

  def do_PRON(self, token):
    if token.dep_ == "nsubj" and token.tag_ == "PRP":
      self.pronouns_count += 1
    if token.dep_ == "dobj" and token.tag_ == "PRP":
      self.pronouns_dobj_count += 1

