
St. Jude makes a unique promise as part of its fundraising: “Families never receive a bill from St. Jude for treatment, travel, housing or food — because all a family should worry about is helping their child live.”
