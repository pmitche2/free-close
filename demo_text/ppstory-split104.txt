
Taylr and Treg said the doctors at St. Jude are “amazing” and they’re grateful for their son’s care. But they bristled at the assumption that it was covered by the hospital’s charity. The family’s insurance paid a substantial part of the bills.
