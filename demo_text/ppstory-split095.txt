
The hospital agreed to allow all five family members to stay for free at St. Jude if they bunked together in a single room. It assigned them a spot in Tri Delta Place, its hotel-like short-term patient residence on the campus. Tri Delta is set up for visits of up to seven days, according to the hospital’s guide for volunteers, but the Murphys were there for almost 50.
