
Even parents with stable jobs and private health insurance often take on debt and need outside help.
