
David Clark, a pediatrician and former longtime chairperson of pediatrics at Albany Medical Center in New York, said St. Jude raises tens of thousands of dollars in his region that does little to benefit the children with cancer in his area since almost all are treated locally. ALSAC has a fundraising office located a few miles from Albany Medical.
