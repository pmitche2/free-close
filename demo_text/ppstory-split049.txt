
IRS rules do not limit the size of a nonprofit’s reserves, and experts on charitable finance differ on best practices.
