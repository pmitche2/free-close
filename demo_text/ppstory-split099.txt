
Fearful of being evicted or having their car repossessed, Taylr said she asked a St. Jude social worker for assistance. The social worker helped her apply for grants from other charities. Taylr said the B+ Foundation paid their rent one month, which ensured they’d have a home to return to.
