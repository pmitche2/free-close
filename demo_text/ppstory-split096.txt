
Taylr said the unit at Tri Delta had no oven or stove and St. Jude provided no grocery money, instead allotting them a $50-per-day credit at the hospital cafeteria, Kay Kafe — not enough to feed the family of five. As the weeks wore on, the Murphys split grilled cheese sandwiches and paid for food out of pocket.
