import spacy
import glob
import os
import pandas as pd

from tabulate import tabulate
from counter import Counter

nlp = spacy.load("en_core_web_sm")
report = {
  "Filename": glob.glob("./demo_text/*.txt"),
  "Names": [],
  "Pronouns(S)": [],
  "Pronouns(O)": []
}

for filename in report["Filename"]:
  f = open(filename, "r")
  doc = nlp(f.read())
  counter = Counter()
  counter.document(doc)
  report["Names"].append(counter.name_count)
  report["Pronouns(S)"].append(counter.pronouns_count)
  report["Pronouns(O)"].append(counter.pronouns_dobj_count)

df = pd.DataFrame(report)

print(tabulate(report, headers=["Filename", "Names", "Pronouns", "Pron OBJ"]))

print(df.describe())
