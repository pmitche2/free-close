import spacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("This is a sentence containing the name Paul Mitchell. He is fine using it like this.")

for token in doc:
    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
            token.shape_, token.is_alpha, token.is_stop)


